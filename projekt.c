#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <err.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <fcntl.h>

#define FIFO "fifo.t"
#define PLIK "plik.t"

int ppid, pid1, pid2, pid3;
int pids[4];

union semun {
        int val;
        struct semid_ds *buf;
        unsigned short int *array;
        struct seminfo *__buf;
};



int semlock(int semid) {
	struct sembuf opr;	

    opr.sem_num =  0;
    opr.sem_op  = -1;       
    opr.sem_flg =  0;       
            
	if (semop(semid, &opr, 1) == -1){
    	warn("Blad blokowania semafora!");
    	return 0;
    }else{
    	return 1;
    }
}



int semunlock(int semid) {
	struct sembuf opr;	
	
    opr.sem_num = 0;
    opr.sem_op  = 1;
    opr.sem_flg = 0;
            
	if (semop(semid, &opr, 1) == -1){
    	warn("Blad odblokowania semafora!");
    	return 0;
    }else{
    	return 1;
    }
}


void sigHandler(int signo) {
	switch(signo) {
		case SIGINT:
			remove(FIFO);
			remove(PLIK);
			kill(pid1, 9);
			kill(pid2, 9);
			kill(pid3, 9);
			kill(ppid, 9);
			break;
		case SIGSTOP:
			kill(pid1, SIGSTOP);
			kill(pid2, SIGSTOP);
			kill(pid3, SIGSTOP);
			kill(ppid, SIGSTOP);
			break;
		case SIGCONT:
			kill(pid1, SIGCONT);
			kill(pid2, SIGCONT);
			kill(pid3, SIGCONT);
			kill(ppid, SIGCONT);
			break;
	}
}


void savePid(int ppid, int pid1, int pid2, int pid3) {
	FILE *pidFile = fopen("pids", "w+");
	int i = 0;

	fprintf(pidFile, "%d ", ppid);
	fprintf(pidFile, "%d ", pid1);
	fprintf(pidFile, "%d ", pid2);
	fprintf(pidFile, "%d ", pid3);
	fclose(pidFile);
}

void readPid() {
	FILE *readPidFile = fopen("pids", "r");
	int i = 0;
	int c;
	for(i = 0; i < 4; i++) {
		fscanf(readPidFile, "%d", &c);
		pids[i] = c;
		printf("%d\n", pids[i]);
	}
	fclose(readPidFile);
}
	

int main(int argc, char **argv) {

	FILE *file;

	file = fopen(PLIK, "w+");
	fclose(file);

	umask(0);
	mkfifo(FIFO, 0666);
	ppid = getpid();

	signal(SIGINT, sigHandler);
//	signal(SIGSTOP, sigHandler);
//	signal(SIGCONT, sigHandler);

	key_t key1, key2, key3;
	int semid1, semid2, semid3;

	union semun ctl;

	if((key1 = ftok(".", 'A')) == -1) {
		errx(1, "Blad tworzenia klucza!");
	}
	if((semid1 = semget(key1, 1, IPC_CREAT | 0600)) == -1) {
		errx(2, "Blad tworzenia semafora!");
	}
	ctl.val = 1;
	if(semctl(semid1, 0, SETVAL, ctl) == -1) {
		errx(3, "Blad ustawiania semafora");
	}

	if((key2 = ftok(".", 'B')) == -1) {
		errx(1, "Blad tworzenia klucza!");
	}
	if((semid2 = semget(key2, 1, IPC_CREAT | 0600)) == -1) {
		errx(2, "Blad tworzenia semafora!");
	}
	ctl.val = 1;
	if(semctl(semid2, 0, SETVAL, ctl) == -1) {
		errx(3, "Blad ustawiania semafora");
	}
		
	if((key3 = ftok(".", 'C')) == -1) {
		errx(1, "Blad tworzenia klucza!");
	}
	if((semid3 = semget(key3, 1, IPC_CREAT | 0600)) == -1) {
		errx(2, "Blad tworzenia semafora!");
	}
	ctl.val = 1;
	if(semctl(semid3, 0, SETVAL, ctl) == -1) {
		errx(3, "Blad ustawiania semafora");
	}
			printf("poczatek\n");
	semlock(semid2);
	semlock(semid1);
	printf("przed P1\n");
	FILE *input = stdin;
	if(argc == 1) input = stdin;
	else if (argc == 2) input = fopen(argv[1], "r");

	if((pid1 = fork()) == 0) {
		int fifo;
		char buffer1[100] = {0};
		fifo = open(FIFO, O_WRONLY);
		while(1) {
				semlock(semid3);
				printf("P1\n");
				if(feof(input)) kill(ppid, 2);
				fscanf(input, "%s", buffer1);
				write(fifo, buffer1, sizeof(buffer1));
				semunlock(semid1);
		}
	}

	if((pid2 = fork()) == 0) {
		int fifoRead;
		char buffer2[100] = {0};
		FILE *plik = NULL;
		int i = 0;
		fifoRead = open(FIFO, O_RDONLY);
		while(1) {
			semlock(semid1);
			int n = read(fifoRead, buffer2, sizeof(buffer2));
			plik = fopen(PLIK, "w");
			int length = strlen(buffer2);
			for(i = 0; i < length; i++) {
				if(i > 0) {
					if(!(i%15)) fprintf(plik, "\n");
				}
				fprintf(plik, "%02X ", buffer2[i]);
			}				
			fclose(plik);
			semunlock(semid2);
		}
	}

	if((pid3 = fork()) == 0) {
		FILE *plikRead;
		char buffer3[100] = {0};
		while(1) {
			semlock(semid2);
			plikRead = fopen(PLIK, "r");
			while(fgets(buffer3, 100, plikRead)) {
				fprintf(stderr, "%s", buffer3);
			}
			fclose(plikRead);
			semunlock(semid3);
		}
	}
//	printf("ppid: %d, pid1: %d, pid2: %d, pid3: %d", ppid, pid1, pid2, pid3);
//	savePid(ppid, pid1, pid2, pid3);
//	readPid();
	for(;;) pause();
}